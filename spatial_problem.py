from cheb import cheb
import numpy as np
from numpy import cosh, tanh
from scipy.optimize import minimize
from oct2py import Oct2Py
from cmath import sqrt
import matplotlib.pyplot as plt
plt.style.use('bmh')

"""
Although this is a Python script it uses the Octave function polyeig. 
You must have Octave and the Python module oct2py installed. 
"""

oc = Oct2Py()
j = sqrt(-1)

def sech(r):
	if not hasattr(r, '__len__'):
		if abs(r) < 700:
			return 1 / cosh(r)
		else:
			return 0
	else:
		y = np.zeros( len(r) )
		idx = abs(r) < 700
		y[idx] = 1 / cosh(r[idx])  

		return y

def derivatives(N):
	D, r = cheb(N)
	r.shape = (N+1,)
	D2 = np.dot(D, D)
	I = np.eye(N+1)
	Z = np.zeros( (N+1, N+1) )

	return I, Z, r,  D, D2

def mean_flow(r, rf, delta, r0):
	r = r * r0 / 2 + 1e-15
	b = 0.25 * rf / delta
	U = 0.5 * (1 + tanh(b * (rf/r - r/rf)))
	dU = -0.25 * r0 * b * (rf/r**2 + 1 / rf) * sech(b * (rf/r - r/rf))**2
	d2U = 0.25 * r0**2 * (rf * b * sech(b * (rf/r - r/rf))**2 / r**3 - 
		b**2 * (rf/r**2 + 1 / rf)**2 * tanh(b * (rf/r - r/rf)) * 
		sech(b * (rf/r - r/rf))**2)

	return U, dU, d2U

def calc_spatial_eig_mats(s, I, D, D2, omega, U, dU, d2U):
	r = s + 1
	A0 = omega * (r**2 * D2 + r * D - I)
	A1 = - U * (r**2 * D2 + r * D - I)  - r * dU * I + r**2 * d2U * I
	A2 = -omega * r**2 * I 
	A3 = r**2 * U * I

	return A0, A1, A2, A3

def enforce_bcs(eig_mats):
	return [mat[1:-1,1:-1] for mat in eig_mats]

def add_bcs(vec):
	return np.hstack([np.zeros(1),vec,np.zeros(1)])

def add_boundary(vec):
	v = vec[0:N-1]
	z = np.array([0])
	v = np.hstack((z, v, z))

	return v

def calc_alpha(omega, delta, rf, s, I, D, D2, U, dU, d2U):
	eig_mats = calc_spatial_eig_mats(s, I, D, D2, omega, U, dU, d2U)
	eig_mats = enforce_bcs(eig_mats)
	A0, A1, A2, A3 = eig_mats
	vecs, alpha = oc.polyeig(A0, A1, A2, A3)
	alpha = alpha[0:len(s)-2]
	vecs = np.vstack([add_bcs(vec) for vec in vecs[:,0:len(s)-2].T])

	return vecs, alpha.flatten()

def calc_most_unstable_alpha(omega, delta, rf, s, I, D, D2, U, dU, d2U):
	vecs, alpha = calc_alpha(omega, delta, rf, s, I, D, D2, U, dU, d2U)
	alpha_max = alpha[np.argmax(alpha.imag)]
	vec_max = vecs[np.argmax(alpha.imag)]

	return vec_max, alpha_max

def calc_most_unstable_alpha_imag(omega, delta, rf, s, I, D, D2, U, dU, d2U):
	print(omega)
	vec_max, alpha_max = calc_most_unstable_alpha(omega, delta, rf, s, I, D, D2, U, dU, d2U)
	
	return alpha_max.imag

def plot_alpha(alpha, fig=None, ax=None):
	alpha = alpha[abs(alpha.imag)>1e-12]
	if ax is None:
		fig, ax = plt.subplots()
	ax.plot(alpha.real, alpha.imag, 'o')[0]
	ax.set_xlabel('Re(alpha')

	return fig, ax


N = 200
I, Z, s, D, D2 = derivatives(N)
rf = 0.001
delta = 0.00015998976065531806
r0 = 0.003
L = 0.004
T = 10
alpha = 0.01
U, dU, d2U = mean_flow(s+1, rf, delta, r0)

def vary_omega(omega):
	vecs, alpha = calc_alpha(omega, delta, rf, s, I, D, D2, U, dU, d2U)
	fig, ax = plot_alpha(alpha)

#omega_max = minimize(calc_most_unstable_alpha_imag, x0=50, args=(delta, rf, s, I, D, D2, U, dU, d2U)).x[0]

omega_range = np.arange(0, 200, 10)
alpha_unstable = np.array([calc_most_unstable_alpha_imag(omega, delta, rf, s, 
	I, D, D2, U, dU, d2U) for omega in omega_range])


fig, axs = plt.subplots(1,2)
axs[0].plot(alpha.real, alpha.imag, 'o')
axs[0].set_ylabel('Im(alpha)')
axs[1].set_ylabel('Re(alpha)')

#setup t and omega coords
dt = 0.01
t_range = np.arange(0, T, dt)
omega_range = np.fft.fftfreq(len(t_range), dt)
domega = diff(omega_range)[0]

eigs_unstable = np.array([calc_eigs_unstable(alpha, delta, rf, U, dU, d2U) for alpha in alpha_range])
vu = np.array([eig[0] for eig in eigs_unstable])
omegau = np.array([eig[1] for eig in eigs_unstable])
#alpha_max = minimize(calc_omega_unstable_imag, x0=2.23, args=(delta, rf, U, dU, d2U)).x[0]
alpha_max = alpha_range[np.argmax(omegau.imag)]
omega_max = calc_eigs_unstable(alpha_max, delta, rf, U, dU, d2U)
