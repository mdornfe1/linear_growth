from numpy import cos, ones, diag, arange, tile, eye
from math import pi

def cheb(N):
	n = arange(0, N+1, 1)
	n.shape = (N+1,1)
	x = cos(pi * n / N)
	c = ones((N+1,1))
	c[-1] = 2
	c[0] = 2
	c *= (-1) ** n 
	X = tile(x, (1, N+1))
	dX = X - X.transpose()
	D  = ( c / c.transpose() ) / ( dX + eye(N+1) ) #off diagonal elements
	D = D - diag( sum( D.transpose() , 0 ) ) #diagonal elements
	x.shape = (N+1,)

	return D, x
